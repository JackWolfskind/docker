Swagger-Editor
==============

``
docker pull swaggerapi/swagger-editor
docker run -d -p 80:8080 swaggerapi/swagger-editor

``

Swagger-UI
==============

``
docker pull swaggerapi/swagger-UI
docker run -p 6010:8080 -e BASE_URL=/swagger -e SWAGGER_JSON=/out/swagger.json -v "$PWD":/out swaggerapi/swagger-ui

``